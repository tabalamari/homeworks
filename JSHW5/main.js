function createNewUser() {
    let newUser = {

        set firstName(fn) {
            this._firstName = fn;
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(ln) {
            this._lastName = ln;
        },
        get lastName() {
            return this._lastName;
        },

        set birthday(bd) {

            const splitted = bd.split("."); //разбивает объект String на массив строк путём разделения строки указанной подстрокой
            const reversed = splitted.reverse();
            const joined = reversed.join("-");
            this._birthday = new Date(joined);

        },
        get birthday() {
            return this._birthday;
        },

        getLogin: function () {
            let abbrFN = this.firstName[0].toLowerCase();
            let abbrLN = this.lastName.toLowerCase();
            return abbrFN + abbrLN;
        },

        getAge: function () {
            const nowDate = new Date();
            const nowDateJear = nowDate.getFullYear();
            const bDateJear = this.birthday.getFullYear();
            const age = nowDateJear - bDateJear;

            const nowDateMonth = nowDate.getMonth();
            const bDateMonth = this.birthday.getMonth();
            if (nowDateMonth < bDateMonth) {
                return age - 1;
            } else if (nowDateMonth === bDateMonth) {
                const nowDateDay = nowDate.getDate();
                const bDateDay = this.birthday.getDate();
                if (nowDateDay < bDateDay) {
                    return age - 1;
                } else if (nowDateDay === bDateDay) {
                    return age;
                } else {
                    return age;
                }

            } else {
                return age;
            }
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();

        },
    };


    newUser.firstName = prompt("Put your first name", "your first name:");
    newUser.lastName = prompt("Put your last name", "last name:");
    newUser.birthday = prompt("Put your date of birth", "date of birth: dd.mm.yyyy:");


    return newUser;
}

let usr = createNewUser();
console.log(usr.getLogin());
console.log(usr.getAge());
console.log(usr.getPassword());




