let tab = function () {
    let tabNav = document.querySelectorAll('.our-services-name'),
        tabContent = document.querySelectorAll('.our-services-content'),
        tabName;

    tabNav.forEach(item => {
        item.addEventListener('click', selectTabNav)
    });

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove('is-active');
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
        })

    }
};
tab();


let loadMore = function () {

    let loadMoreButton = document.querySelector('.amazing-work-button');
    loadMoreButton.addEventListener('click', loadMoreButtonClick);

    function loadMoreButtonClick() {

        // let newElem = getNewElem();
        let newElem = getNewElemByFilter("landing-page");
        let imgContainer = document.querySelector('.amazing-work-images-container');
        imgContainer.appendChild(newElem);
        loadMoreButton.parentNode.removeChild(loadMoreButton);


    }
};



function getNewElemByFilter(filter) {
    let newElem = document.createElement('div');
    let txt = '<div class="amazing-work-images">';
    let counter = 1;
    for (let i = 0; i < 4; i++) {
        txt += '<div>';
        for (let j = 0; j < 3; j++) {
            txt += getImageDiv(filter, counter);
            counter++;
        }
        txt += '</div>';
    }
    txt += '</div>';


    newElem.innerHTML = txt;

    return newElem;

}

function getImageDiv(name, counter) {
    return `<div class="amazing-work-block"><img src="images/amazing-work/${name}${counter}.jpg" alt="Here is a image for amazing-work"><div class="block"><span class="block-text">creative design</span>Web Design</div></div>`;

}

function getNewElemByFilterALl() {
    let mass = ["graphic-design", "landing-page", "web-design", "wordpress"];

    let newElem = document.createElement('div');
    let txt = '<div class="amazing-work-images">';
    let counterOfImage = 1;
    let counter = 0;
    for (let i = 0; i < 4; i++) {
        if (counterOfImage >= 12) {
            break;
        }
        txt += '<div>';
        for (let j = 0; j < 3; j++) {
            counter++;
            txt += getImageDiv(mass[i], counter);
            // for (let k = 0; k < mass.length; k++) {
            //     txt += getImageDiv(mass[k], counter);

            // counterOfImage++;
            // }
        }
        txt += '</div>';

    }
    txt += '</div>';


    newElem.innerHTML = txt;

    return newElem;


}

function getNewElemWebDes() {

    let block = getNewElemByFilter("web-design");
    let imgContainer = document.querySelector('.amazing-work-images-container');
    imgContainer.innerHTML = '';
    imgContainer.appendChild(block);

}

function getNewElemGrDes() {

    let block = getNewElemByFilter("graphic-design");
    let imgContainer = document.querySelector('.amazing-work-images-container');
    imgContainer.innerHTML = '';
    imgContainer.appendChild(block);

}

function getNewElemLandingPage() {

    let block = getNewElemByFilter("landing-page");
    let imgContainer = document.querySelector('.amazing-work-images-container');
    imgContainer.innerHTML = '';
    imgContainer.appendChild(block);

}

function getNewElemWordpress() {

    let block = getNewElemByFilter("wordpress");
    let imgContainer = document.querySelector('.amazing-work-images-container');
    imgContainer.innerHTML = '';
    imgContainer.appendChild(block);

}

function getNewElementAll() {
    let block = getNewElemByFilterALl();
    let imgContainer = document.querySelector('.amazing-work-images-container');
    imgContainer.innerHTML = '';
    imgContainer.appendChild(block);

}

function addFilters() {
    let grDesButt = document.getElementById('web-design');
    grDesButt.addEventListener("click", getNewElemWebDes);
    let grDesButt2 = document.getElementById('graphic-desing');
    grDesButt2.addEventListener("click", getNewElemGrDes);
    let grDesButt3 = document.getElementById('landing-page');
    grDesButt3.addEventListener("click", getNewElemLandingPage);
    let grDesButt4 = document.getElementById('wordpress');
    grDesButt4.addEventListener("click", getNewElemWordpress);
    let allButton = document.getElementById('all');
    allButton.addEventListener("click", getNewElementAll);

}

addFilters();

loadMore();




