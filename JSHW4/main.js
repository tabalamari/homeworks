function createNewUser() {
    let newUser = { // объявили переменную и в нее поместим созданный объект
        set firstName(fn){ // объявляем сеттер для свойства firstName, сеттер принимает как параметр значение имени(н-р "Вася")
            this._firstName = fn; // собственно свойство в котором будем хранить имя. Подчеркивание значит, что это спрятанное свойство, трогать его не надо(Кантор)
        },
        get firstName() { //объявляем геттер для свойства firstName,
            return this._firstName; // геттер возвращает имя пользователя. Подчеркивание начит, что это спрятанное свойство, трогать его не надо(Кантор)
        },
        set lastName(ln) {
            this._lastName = ln;
        },
        get lastName() {
            return this._lastName;
        },

        getLogin: function () {
            let abbrFN = this.firstName[0].toLowerCase();
            let abbrLN = this.lastName.toLowerCase();
            return abbrFN + abbrLN;
        }
    };

    const fName = prompt("Put your first name", "your first name");
    const lName = prompt("Put your last name", "last name");
    newUser.firstName = fName;
    newUser.lastName = lName;
    return newUser;
}

let usr = createNewUser();
console.log(usr.getLogin());
