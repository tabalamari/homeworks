function filterBy(arr, type){
    return arr.filter(
        function (item){
            return type !== typeof item;
        }
    );
}

arr = [1,'1',null];
console.log('before:');
arr.forEach(function(item){console.log(item)});
res = filterBy(arr, 'string');
console.log('after:');
res.forEach(function(item){console.log(item)});
