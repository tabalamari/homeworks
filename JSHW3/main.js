// спросить первое число
let num1 =  0;
num1 = getNumber();
// спросить операцию
let oper = "+";
    oper = getOperation();
// спросить второе число
let num2 = 0;
num2 = getNumber();
// посчитать
const result = calc(num1, oper, num2);
// сказать ответ
alert(result);
// Функция, которая считает
function calc(operand1, operation, operand2) {
    if (operation === "+") {
        return operand1 + operand2;
    } else if (operation === "-") {
        return operand1 - operand2;
    } else if (operation === "*") {
        return operand1 * operand2;
    } else if (operation === "/") {
        return operand1 / operand2;
    }
}
//Функция, которая просит ввести значение и проверяет его на число
function getNumber() {
    let num = (+prompt("Put a number"));
    while (!isFinite(num) || num !== Math.trunc(num)) {
        num = (+prompt("Put a number"))
    }
    return num;
}
// Функция, которая просит ввести матем.операцию и проверяет операция ли это
function getOperation() {
    let oper = (prompt("Put an operation"));
    while (oper !== "+" && oper !== "-" && oper !== "*" && oper !== "/"){
        oper = (prompt("Put an operation"))
    }
    return oper;
}
