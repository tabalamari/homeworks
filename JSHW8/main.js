let inpPrice = document.querySelector("input");
let pleaseTag = document.createElement("span");

inpPrice.addEventListener('blur', blur);
inpPrice.addEventListener('focus', focus);

let spanTag = document.createElement("span");
let inpTag = document.createElement("input");

function focus() {
    inpPrice.style.borderColor = 'green';
    pleaseTag.remove();
    inpPrice.value = "";

}


function blur() {
    let content = inpPrice.value;
    inpPrice.style.borderColor = 'black';
    if (content > 0 && isFinite(content)) {
        inpPrice.style.color = 'green';

        spanTag.innerHTML = "Текущая цена: $" + content;
        document.body.prepend(spanTag);
        spanTag.style.border = "1px solid black";
        spanTag.style.padding = "10px";
        spanTag.style.margin = "40px";

        inpTag.type = "checkbox";
        document.body.prepend(inpTag);
        inpTag.addEventListener("click", function () {
            spanTag.remove();
            inpTag.remove();
            inpPrice.value = ""
        });


    } else {
        inpPrice.style.borderColor = 'red';
        pleaseTag.innerHTML = "Please enter correct price";
        document.body.append(pleaseTag);
        pleaseTag.style.margin = "50px";
        spanTag.remove();
    }
}

