
function timer() {
    const timer = document.getElementById('timer');
    timer.innerText = parseInt(timer.innerText) - 1;
    if (timer.innerText < 0) {
        window.stop();
        document.body.innerHTML = "";
    }
}

function arrToListItems(arr) {
    if (Array.isArray(arr)) {
        const newArr = arr.map(word => `<li>${word}</li>`).join('');
        return `<ul>${newArr}</ul>`;
    } else {
        return `<li>${arr}</li>`;
    }
}

const arr = ["Kyiv", "Dnipro", "Odessa", ["25", "55", "59"], "Lviv"];

const myList = document.getElementById("list");
myList.innerHTML = arr.map(arrToListItems).join('');
window.timerId = window.setInterval(timer, 1000);